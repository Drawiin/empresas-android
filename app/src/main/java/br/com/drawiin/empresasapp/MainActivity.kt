package br.com.drawiin.empresasapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import br.com.drawiin.empresasapp.ui.screens.enterprise.EnterpriseScreen
import br.com.drawiin.empresasapp.ui.screens.home.HomeScreen
import br.com.drawiin.empresasapp.ui.screens.home.HomeViewModel
import br.com.drawiin.empresasapp.ui.screens.login.LoginScreen
import br.com.drawiin.empresasapp.ui.screens.login.LoginViewModel
import br.com.drawiin.empresasapp.ui.theme.EmpresasAppTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            EmpresasAppTheme {
                NavHost(
                    navController = navController,
                    startDestination = Routes.Login.route
                ) {
                    composable(Routes.Login.route) {
                        val loginViewModel: LoginViewModel = hiltViewModel()
                        LoginScreen(loginViewModel) {
                            navController.navigate(Routes.Home.route)
                        }
                    }
                    composable(Routes.Home.route) {
                        val homeViewModel: HomeViewModel = hiltViewModel()
                        HomeScreen(homeViewModel)
                    }
                    composable(Routes.EnterpriseDetails.route) { backStackEntry ->
                        val id = backStackEntry.arguments?.getString("id")
                        EnterpriseScreen()
                    }
                }

            }
        }
    }
}

sealed class Routes(val route: String) {
    object Home : Routes("home")
    object Login : Routes("login")
    object EnterpriseDetails : Routes("enterprise/{id}") {
        fun createRoute(id: Int) = "enterprise/${id}"
    }
}

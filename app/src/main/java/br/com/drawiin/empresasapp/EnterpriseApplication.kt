package br.com.drawiin.empresasapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class EnterpriseApplication: Application() {
}
package br.com.drawiin.empresasapp.network.util

import android.app.Application
import android.content.Context
import androidx.core.content.edit
import br.com.drawiin.empresasapp.core.constants.API_ENDPOINT_NAME
import br.com.drawiin.empresasapp.core.constants.API_VERSION_NAME
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.math.BigInteger
import java.security.MessageDigest
import javax.inject.Inject
import javax.inject.Named

class AuthenticationHandler @Inject constructor(private val context: Application) {

    private val serializer by lazy {
        Json {
            isLenient = true
        }
    }

    val credentials: Credentials?
        get() {
            val preferences = context.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE)
            return try {
                serializer.decodeFromString<Credentials>(
                    preferences.getString(CREDENTIALS_NAME, "") ?: ""
                )
            } catch (e: Exception) {
                null
            }
        }

    fun saveCredentials(credentials: Credentials) {
        val preferences = context.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE)
        val string = serializer.encodeToString(credentials)

        preferences.edit {
            putString(CREDENTIALS_NAME, string)
        }
    }

    companion object {
        const val CREDENTIALS_NAME = "CREDENTIALS_NAME"
        const val APP_NAME = "ENTERPRISE_APP "
    }
}
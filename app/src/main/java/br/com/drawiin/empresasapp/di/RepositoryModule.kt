package br.com.drawiin.empresasapp.di

import br.com.drawiin.empresasapp.domain.repository.EnterpriseRepository
import br.com.drawiin.empresasapp.repository.DefaultEnterpriseRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {
    @Singleton
    @Binds
    fun bindsEnterpriseRepository(defaultComicRepository: DefaultEnterpriseRepository): EnterpriseRepository
}
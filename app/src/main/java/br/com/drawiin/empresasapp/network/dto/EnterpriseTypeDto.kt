package br.com.drawiin.empresasapp.network.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class EnterpriseTypeDto(
    @SerialName("enterprise_type_name")
    val enterpriseTypeName: String?,
    @SerialName("id")
    val id: Int?
)
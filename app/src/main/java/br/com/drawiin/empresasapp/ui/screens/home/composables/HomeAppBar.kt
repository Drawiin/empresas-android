package br.com.drawiin.empresasapp.ui.screens.home.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import br.com.drawiin.empresasapp.R
import br.com.drawiin.empresasapp.ui.theme.mediumPink

@Composable
fun HomeAppBar(onSearchClicked: () -> Unit) {
    TopAppBar(
        backgroundColor = mediumPink,
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(modifier = Modifier)
            Image(
                painter = painterResource(id = R.drawable.logo_nav),
                contentDescription = stringResource(
                    id = R.string.ioasys
                )
            )
            IconButton(onClick = onSearchClicked) {
                Icon(
                    modifier = Modifier.size(30.dp),
                    painter = painterResource(id = R.drawable.ic_search_copy),
                    tint = Color.White,
                    contentDescription = stringResource(
                        id = R.string.description_search
                    )
                )
            }
        }
    }
}
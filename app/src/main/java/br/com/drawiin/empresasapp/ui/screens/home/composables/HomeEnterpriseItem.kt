package br.com.drawiin.empresasapp.ui.screens.home.composables

import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import br.com.drawiin.empresasapp.network.dto.EnterpriseDto
import br.com.drawiin.empresasapp.ui.screens.composables.NetworkImage
import br.com.drawiin.empresasapp.ui.theme.indigo

@Composable
fun HomeEnterpriseItem(item: EnterpriseDto) {
    Surface {
        Row(
            modifier = Modifier
                .height(150.dp)
                .padding(8.dp)
                .fillMaxWidth()
        ) {
            NetworkImage(
                modifier = Modifier.fillMaxWidth(0.35f),
                url =
                "https://empresas.ioasys.com.br" + item.photo, contentDescription = null
            )
            Spacer(modifier = Modifier.width(8.dp))
            Column(
                modifier = Modifier.fillMaxWidth(0.65f)
            ) {
                Text(
                    item.enterpriseName ?: "", style = TextStyle(
                        fontSize = 17.sp,
                        color = indigo,
                        fontWeight = FontWeight.Bold
                    )
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    item.description ?: "",
                    modifier = Modifier.fillMaxWidth(),
                    overflow = TextOverflow.Clip,
                    maxLines = 4
                )
            }
        }
    }
}
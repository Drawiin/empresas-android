package br.com.drawiin.empresasapp.domain.model

data class LoginCredentials(val email: String, val password: String) {
    fun isEmpty() = email.isEmpty() || password.isEmpty()
}
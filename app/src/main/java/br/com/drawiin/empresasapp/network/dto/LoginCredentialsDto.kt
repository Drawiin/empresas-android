package br.com.drawiin.empresasapp.network.dto


import br.com.drawiin.empresasapp.domain.model.LoginCredentials
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LoginCredentialsDto(
    @SerialName("email")
    val email: String?,
    @SerialName("password")
    val password: String?
) {
    companion object {
        fun fromDomainModel(credentials: LoginCredentials) = LoginCredentialsDto(
            email = credentials.email,
            password = credentials.password
        )
    }
}
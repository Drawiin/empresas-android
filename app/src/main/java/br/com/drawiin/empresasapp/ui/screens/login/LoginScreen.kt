package br.com.drawiin.empresasapp.ui.screens.login

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import br.com.drawiin.empresasapp.R

@Composable
fun LoginScreen(viewModel: LoginViewModel, onSuccess: () -> Unit) {
    val uiState = viewModel.uiState.collectAsState()

    when (val state = uiState.value) {
        is LoginUiState.Stated -> Body(viewModel, state)
        LoginUiState.Success -> onSuccess()
    }
}

@Composable
fun Body(viewModel: LoginViewModel, state: LoginUiState.Stated) {
    Scaffold(
        backgroundColor = Color.White
    ) {
        Column(
            Modifier
                .fillMaxSize()
                .padding(top = 40.dp, start = 45.dp, end = 45.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Image(painter = painterResource(R.drawable.logo_home), contentDescription = null)
            Spacer(modifier = Modifier.height(48.dp))
            Text(
                text = stringResource(id = R.string.wellcome),
                style = TextStyle(
                    fontSize = 20.sp,
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold
                )
            )
            Spacer(modifier = Modifier.height(48.dp))
            Text(
                text = stringResource(id = R.string.loren),
                style = TextStyle(fontSize = 16.sp, textAlign = TextAlign.Center)
            )
            Spacer(modifier = Modifier.height(48.dp))
            TextField(
                value = state.credential.email,
                onValueChange = { newValue ->
                    viewModel.triggerEvent(
                        LoginUiEvents.ChangeEmail(
                            newValue
                        )
                    )
                },
                placeholder = {
                    Text(
                        text = stringResource(id = R.string.hint_email),
                        style = TextStyle(fontSize = 17.sp, color = Color.Gray)
                    )
                },
                leadingIcon = {
                    Image(
                        modifier = Modifier.size(20.dp),
                        painter = painterResource(id = R.drawable.ic_email),
                        contentDescription = null
                    )
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    focusedIndicatorColor = Color.Gray,
                    unfocusedIndicatorColor = Color.Gray
                )
            )
            TextField(
                value = state.credential.password,
                visualTransformation = PasswordVisualTransformation(),
                onValueChange = { newValue ->
                    viewModel.triggerEvent(
                        LoginUiEvents.ChangePassword(
                            newValue
                        )
                    )
                },
                placeholder = {
                    Text(
                        text = stringResource(id = R.string.hint_password),
                        style = TextStyle(fontSize = 17.sp, color = Color.Gray)
                    )
                },
                leadingIcon = {
                    Image(
                        modifier = Modifier.size(20.dp),
                        painter = painterResource(id = R.drawable.ic_cadeado),
                        contentDescription = null
                    )
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    focusedIndicatorColor = Color.Gray,
                    unfocusedIndicatorColor = Color.Gray
                )
            )
            Spacer(modifier = Modifier.height(32.dp))
            Button(
                contentPadding = PaddingValues(16.dp),
                modifier = Modifier
                    .fillMaxWidth(),
                onClick = { viewModel.triggerEvent(LoginUiEvents.DoLogin) }
            ) {
                Text(text = stringResource(id = R.string.enter))
            }
        }
    }
}

@Preview
@Composable
fun Preview() {

}
package br.com.drawiin.empresasapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val blue = Color(0xFF57bbbc)
val mediumPink = Color(0xFFee4c77)
val darkPink = Color(0xFFbc3c5e)
val rouge = Color(0xFF991237)
val indigo = Color(0xFF1a0e49)
package br.com.drawiin.empresasapp.domain.repository

import br.com.drawiin.empresasapp.core.arch.Either
import br.com.drawiin.empresasapp.core.error.Failure
import br.com.drawiin.empresasapp.domain.model.LoginCredentials
import br.com.drawiin.empresasapp.network.dto.EnterpriseDto
import br.com.drawiin.empresasapp.network.dto.LoginResponseDto

interface EnterpriseRepository {
    suspend fun getEnterprise(id: Int): Either<Failure, Any>
    suspend fun logIn(credentials: LoginCredentials): Either<Failure, LoginResponseDto>
    suspend fun getEnterprises(query: String): Either<Failure, List<EnterpriseDto>>
}
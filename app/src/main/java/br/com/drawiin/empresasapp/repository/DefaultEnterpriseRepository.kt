package br.com.drawiin.empresasapp.repository

import br.com.drawiin.empresasapp.core.arch.Either
import br.com.drawiin.empresasapp.core.error.Failure
import br.com.drawiin.empresasapp.core.extensions.toCustomExceptions
import br.com.drawiin.empresasapp.domain.model.LoginCredentials
import br.com.drawiin.empresasapp.domain.repository.EnterpriseRepository
import br.com.drawiin.empresasapp.network.dto.EnterpriseDto
import br.com.drawiin.empresasapp.network.dto.LoginCredentialsDto
import br.com.drawiin.empresasapp.network.dto.LoginResponseDto
import br.com.drawiin.empresasapp.network.service.EnterpriseService
import com.example.comiclover.network.util.NetworkHandler
import javax.inject.Inject

class DefaultEnterpriseRepository @Inject constructor(
    private val enterpriseService: EnterpriseService,
    private val networkHandler: NetworkHandler
) : EnterpriseRepository {
    override suspend fun getEnterprise(id: Int): Either<Failure, Any> {
        return Either.Right(Any())
    }

    override suspend fun logIn(credentials: LoginCredentials): Either<Failure, LoginResponseDto> =
        when (networkHandler.isConnected) {
            true -> try {
                Either.Right(
                    enterpriseService.login(
                        LoginCredentialsDto.fromDomainModel(
                            credentials = credentials
                        )
                    )
                )
            } catch (e: Exception) {
                Either.Left(e.toCustomExceptions())
            }
            else -> Either.Left(Failure.NetworkConnection)
        }


    override suspend fun getEnterprises(query: String): Either<Failure, List<EnterpriseDto>> =
        when (networkHandler.isConnected) {
            true -> try {
                Either.Right(
                    enterpriseService.getEnterprises(query).enterprises
                )
            } catch (e: Exception) {
                Either.Left(e.toCustomExceptions())
            }
            else -> Either.Left(Failure.NetworkConnection)
        }
}
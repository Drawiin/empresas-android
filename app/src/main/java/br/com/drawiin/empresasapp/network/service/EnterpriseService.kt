package br.com.drawiin.empresasapp.network.service

import br.com.drawiin.empresasapp.network.dto.EnterpriseResponseDto
import br.com.drawiin.empresasapp.network.dto.LoginCredentialsDto
import br.com.drawiin.empresasapp.network.dto.LoginResponseDto
import br.com.drawiin.empresasapp.network.util.AuthenticationHandler
import br.com.drawiin.empresasapp.network.util.CredentialNames
import io.ktor.client.*
import io.ktor.client.request.*
import javax.inject.Inject

class EnterpriseService @Inject constructor(
    private val client: HttpClient,
    private val authenticationHandler: AuthenticationHandler
) {
    suspend fun login(loginCredentials: LoginCredentialsDto): LoginResponseDto =
        client.post("users/auth/sign_in") {
            body = loginCredentials
            addCredentials()
        }

    suspend fun getEnterprises(query: String): EnterpriseResponseDto =
        client.get("enterprises") {
            addCredentials()
        }

    private fun HttpRequestBuilder.addCredentials() {
        val credentials = authenticationHandler.credentials
        header(CredentialNames.AccessToken.name, credentials?.accessToken)
        header(CredentialNames.Client.name, credentials?.client)
        header(CredentialNames.Uid.name, credentials?.uid)
    }
}
package br.com.drawiin.empresasapp.ui.screens.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.drawiin.empresasapp.core.arch.onFailure
import br.com.drawiin.empresasapp.core.arch.onSuccess
import br.com.drawiin.empresasapp.domain.model.LoginCredentials
import br.com.drawiin.empresasapp.domain.repository.EnterpriseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    val repository: EnterpriseRepository
) : ViewModel() {
    private val _uiState: MutableStateFlow<LoginUiState> = MutableStateFlow(
        LoginUiState.Stated(
            LoginCredentials(
                email = "",
                password = ""
            )
        )
    )

    val uiState: StateFlow<LoginUiState> = _uiState

    private fun setState(setState: () -> LoginUiState) {
        _uiState.value = setState()
    }

    fun triggerEvent(event: LoginUiEvents) {
        when (event) {
            is LoginUiEvents.ChangeEmail -> setState {
                when (val state = _uiState.value) {
                    is LoginUiState.Stated -> state.copy(
                        credential = state.credential.copy(
                            email = event.email
                        )
                    )
                    else -> state
                }
            }
            is LoginUiEvents.ChangePassword -> setState {
                when (val state = _uiState.value) {
                    is LoginUiState.Stated -> state.copy(
                        credential = state.credential.copy(
                            password = event.password
                        )
                    )
                    else -> state
                }
            }
            is LoginUiEvents.DoLogin -> when (val state = _uiState.value) {
                is LoginUiState.Stated ->
                    if (state.credential.isEmpty().not())
                        viewModelScope.launch(Dispatchers.IO) {
                            repository.logIn(state.credential).onSuccess {
                                setState {
                                    LoginUiState.Success
                                }
                            }.onFailure {

                            }
                        }
            }
        }
    }
}

sealed class LoginUiState {
    data class Stated(val credential: LoginCredentials) : LoginUiState()
    object Success : LoginUiState()
}

sealed class LoginUiEvents {
    data class ChangePassword(val password: String) : LoginUiEvents()
    data class ChangeEmail(val email: String) : LoginUiEvents()
    object DoLogin : LoginUiEvents()
}
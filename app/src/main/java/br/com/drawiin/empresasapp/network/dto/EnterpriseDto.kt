package br.com.drawiin.empresasapp.network.dto


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class EnterpriseDto(
    @SerialName("city")
    val city: String?,
    @SerialName("country")
    val country: String?,
    @SerialName("description")
    val description: String?,
    @SerialName("email_enterprise")
    val emailEnterprise: String?,
    @SerialName("enterprise_name")
    val enterpriseName: String?,
    @SerialName("enterprise_type")
    val enterpriseType: EnterpriseTypeDto?,
    @SerialName("facebook")
    val facebook: String?,
    @SerialName("id")
    val id: Int?,
    @SerialName("linkedin")
    val linkedin: String?,
    @SerialName("own_enterprise")
    val ownEnterprise: Boolean?,
    @SerialName("phone")
    val phone: String?,
    @SerialName("photo")
    val photo: String?,
    @SerialName("share_price")
    val sharePrice: Double?,
    @SerialName("twitter")
    val twitter: String?,
    @SerialName("value")
    val value: Int?
)
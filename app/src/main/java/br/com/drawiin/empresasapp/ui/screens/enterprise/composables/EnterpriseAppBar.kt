package br.com.drawiin.empresasapp.ui.screens.enterprise.composables

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import br.com.drawiin.empresasapp.R
import br.com.drawiin.empresasapp.ui.theme.mediumPink

@Composable
fun EnterpriseAppBar(
    onBackPressed: () -> Unit
) {
    TopAppBar(
        backgroundColor = mediumPink,
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(onClick = onBackPressed) {
                Icon(
                    modifier = Modifier.size(30.dp),
                    painter = painterResource(id = R.drawable.ic_arrow_back),
                    tint = Color.White,
                    contentDescription = stringResource(
                        id = R.string.description_search
                    )
                )
            }
            Spacer(modifier = Modifier)
            Text(
                text = "Empresa 1",
                style = TextStyle(
                    color = Color.White,
                    fontSize = 17.sp
                )
            )
        }
    }
}
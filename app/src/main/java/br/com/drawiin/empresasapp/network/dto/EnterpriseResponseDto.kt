package br.com.drawiin.empresasapp.network.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class EnterpriseResponseDto(
    @SerialName("enterprises")
    val enterprises: List<EnterpriseDto>
)
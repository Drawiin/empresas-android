package br.com.drawiin.empresasapp.ui.screens.home.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import br.com.drawiin.empresasapp.network.dto.EnterpriseDto

@Composable
fun HomeSearchBody(
    query: String,
    onQueryChanged: (String) -> Unit,
    onCancelSearch: () -> Unit,
    searchResult: List<EnterpriseDto>
) {
    Scaffold(
        topBar = {
            HomeSearchBar(
                textValue = query,
                onTextChanged = onQueryChanged,
                onCancelSearch = onCancelSearch,
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.LightGray),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            print("APP_LOG$searchResult")
            LazyColumn(
                modifier = Modifier.padding(16.dp)
            ) {
                items(searchResult) { item ->
                    HomeEnterpriseItem(item = item)
                    Spacer(modifier = Modifier.height(16.dp))
                }
            }
        }
    }
}
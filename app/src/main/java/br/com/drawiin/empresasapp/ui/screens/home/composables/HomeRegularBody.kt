package br.com.drawiin.empresasapp.ui.screens.home.composables

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp
import br.com.drawiin.empresasapp.R

@Composable
fun HomeRegularBody(onSearchClicked: () -> Unit) {
    Scaffold(
        topBar = { HomeAppBar(onSearchClicked = onSearchClicked) }
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = stringResource(id = R.string.click_search),
                style = TextStyle(fontSize = 16.sp)
            )
        }
    }
}
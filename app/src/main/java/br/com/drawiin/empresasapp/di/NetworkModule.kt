package br.com.drawiin.empresasapp.di

import android.util.Log
import br.com.drawiin.empresasapp.BuildConfig
import br.com.drawiin.empresasapp.core.constants.API_ENDPOINT_NAME
import br.com.drawiin.empresasapp.core.constants.API_VERSION_NAME
import br.com.drawiin.empresasapp.core.constants.NETWORK_TIME_OUT
import br.com.drawiin.empresasapp.network.util.AuthenticationHandler
import br.com.drawiin.empresasapp.network.util.CredentialNames
import br.com.drawiin.empresasapp.network.util.Credentials
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.features.observer.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Named(API_ENDPOINT_NAME)
    @Provides
    fun providesApiEndPoint(): String = BuildConfig.API_END_POINT

    @Singleton
    @Named(API_VERSION_NAME)
    @Provides
    fun providesApiVersion(): String = BuildConfig.API_VERSION

    @Singleton
    @Provides
    fun providesJsonSerializer(): JsonSerializer = KotlinxSerializer(Json {
        prettyPrint = true
        isLenient = true
        ignoreUnknownKeys = true
    })

    @ExperimentalSerializationApi
    @Singleton
    @Provides
    fun providesLogger() = object : Logger {
        val format = Json {
            prettyPrint = true
            isLenient = true
            prettyPrintIndent = " "
        }

        override fun log(message: String) {
            val isJson = with(message) { startsWith("{") or startsWith("[") }
            if (isJson)
                try {
                    val json = format.parseToJsonElement(message)
                    val string = format.encodeToString(json)
                    if (string.length >= 4000) {
                        Log.v("App logger =>", "Json to large use print instead")
                    }
                    Log.v("App logger =>", format.encodeToString(json))
                } catch (m: Exception) {
                    Log.v("App logger =>", m.message.toString())
                }
            else
                Log.v("App logger =>", message)
        }
    }

    @Singleton
    @Provides
    fun providesHttpClient(
        jsonSerializer: JsonSerializer,
        appLogger: Logger,
        @Named(API_ENDPOINT_NAME)
        endPoint: String,
        @Named(API_VERSION_NAME)
        version: String,
        authenticationHandler: AuthenticationHandler
    ): HttpClient = HttpClient(Android) {
        HttpResponseValidator {
            validateResponse { response ->
                Log.v("App logger =>", "Saving credentials ${response.headers[CredentialNames.AccessToken.name]}")
                authenticationHandler.saveCredentials(
                    Credentials(
                        accessToken = response.headers[CredentialNames.AccessToken.name],
                        client = response.headers[CredentialNames.Client.name],
                        uid = response.headers[CredentialNames.Uid.name],
                    )
                )
            }
        }

        engine {
            connectTimeout = NETWORK_TIME_OUT
            socketTimeout = NETWORK_TIME_OUT
        }

        install(JsonFeature) {
            serializer = jsonSerializer
        }

        install(Logging) {
            logger = appLogger
            level = if (BuildConfig.DEBUG) LogLevel.ALL else LogLevel.NONE
        }

        install(ResponseObserver) {
            onResponse { response ->
                Log.d("HTTP status:", "${response.status.value}")
            }
        }

        install(DefaultRequest) {

            header(HttpHeaders.ContentType, ContentType.Application.Json)
            host = "${endPoint}/api/${version}"
            url {
                protocol = URLProtocol.HTTPS
            }
        }
    }
}
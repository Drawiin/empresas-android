package br.com.drawiin.empresasapp.ui.screens.enterprise

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import br.com.drawiin.empresasapp.R
import br.com.drawiin.empresasapp.ui.screens.enterprise.composables.EnterpriseAppBar

@Composable
fun EnterpriseScreen() {
    Scaffold(topBar = { EnterpriseAppBar(onBackPressed = {}) }) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(20.dp)
        ) {
            Image(
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(2f),
                painter = painterResource(id = R.drawable.logo_home),
                contentDescription = null
            )
            Spacer(modifier = Modifier.height(20.dp))
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = "Lorem ipsum dolor siserunt mollit anim id est laborum.",
                style = TextStyle(color = Color.Gray, fontSize = 17.sp)
            )
        }
    }
}
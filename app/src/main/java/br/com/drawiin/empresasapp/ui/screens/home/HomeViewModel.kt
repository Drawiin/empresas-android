package br.com.drawiin.empresasapp.ui.screens.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.drawiin.empresasapp.core.arch.onFailure
import br.com.drawiin.empresasapp.core.arch.onSuccess
import br.com.drawiin.empresasapp.network.dto.EnterpriseDto
import br.com.drawiin.empresasapp.repository.DefaultEnterpriseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: DefaultEnterpriseRepository
) : ViewModel() {
    private val _uiState: MutableStateFlow<HomeUiState> = MutableStateFlow(
        HomeUiState.Normal
    )

    val uiState: StateFlow<HomeUiState> = _uiState

    private fun setState(setState: () -> HomeUiState) {
        _uiState.value = setState()
    }

    fun triggerEvent(event: HomeUiEvents) {
        when (event) {
            is HomeUiEvents.StartSearch -> setState {
                HomeUiState.Search("", emptyList())
            }
            is HomeUiEvents.StopSearch -> setState {
                HomeUiState.Normal
            }
            is HomeUiEvents.Query -> when (val state = _uiState.value) {
                is HomeUiState.Search -> viewModelScope.launch(Dispatchers.IO) {
                    setState {
                        state.copy(query = event.query)
                    }
                    repository.getEnterprises(event.query).onSuccess { result ->
                        setState {
                            val newState = _uiState.value as HomeUiState.Search
                            newState.copy(result = result)
                        }
                    }.onFailure {
                        Log.d("HOME", it.toString())
                    }
                }
            }
        }
    }
}

sealed class HomeUiEvents {
    object StartSearch : HomeUiEvents()
    object StopSearch : HomeUiEvents()
    data class Query(val query: String) : HomeUiEvents()
}

sealed class HomeUiState {
    object Normal : HomeUiState()
    data class Search(val query: String, val result: List<EnterpriseDto>) : HomeUiState()
}
package br.com.drawiin.empresasapp.ui.screens.home.composables

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import br.com.drawiin.empresasapp.R
import br.com.drawiin.empresasapp.ui.theme.mediumPink
import br.com.drawiin.empresasapp.ui.theme.rouge

@Composable
fun HomeSearchBar(
    textValue: String,
    onTextChanged: (String) -> Unit,
    onCancelSearch: () -> Unit,
) {
    TopAppBar(
        backgroundColor = mediumPink,
        contentPadding = PaddingValues(4.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            TextField(
                modifier = Modifier.fillMaxSize(),
                value = textValue,
                onValueChange = onTextChanged,
                placeholder = {
                    Text(
                        text = stringResource(id = R.string.hint_search),
                        style = TextStyle(fontSize = 17.sp, color = rouge)
                    )
                },
                trailingIcon = {
                    IconButton(onClick = onCancelSearch) {
                        Icon(
                            modifier = Modifier.size(30.dp),
                            painter = painterResource(id = R.drawable.ic_close),
                            tint = Color.White,
                            contentDescription = stringResource(
                                id = R.string.description_search
                            )
                        )
                    }

                },
                leadingIcon = {
                    Icon(
                        modifier = Modifier.size(30.dp),
                        painter = painterResource(id = R.drawable.ic_search_copy),
                        tint = Color.White,
                        contentDescription = stringResource(
                            id = R.string.description_search
                        )
                    )
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    focusedIndicatorColor = Color.White,
                    unfocusedIndicatorColor = Color.White
                )
            )
        }
    }
}
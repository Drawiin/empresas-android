package br.com.drawiin.empresasapp.network.util

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Credentials(
    @SerialName("access-token")
    val accessToken: String?,
    @SerialName("client")
    val client: String?,
    @SerialName("uid")
    val uid: String?
)

sealed class CredentialNames(val name: String) {
    object AccessToken : CredentialNames("access-token")
    object Client : CredentialNames("client")
    object Uid : CredentialNames("uid")
}

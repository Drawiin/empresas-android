package br.com.drawiin.empresasapp.ui.screens.home

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import br.com.drawiin.empresasapp.ui.screens.home.composables.HomeRegularBody
import br.com.drawiin.empresasapp.ui.screens.home.composables.HomeSearchBody

@Composable
fun HomeScreen(viewModel: HomeViewModel) {
    when (val state = viewModel.uiState.collectAsState().value) {
        is HomeUiState.Normal -> {
            HomeRegularBody {
                viewModel.triggerEvent(HomeUiEvents.StartSearch)
            }
        }
        is HomeUiState.Search -> {
            HomeSearchBody(
                onQueryChanged = { text -> viewModel.triggerEvent(HomeUiEvents.Query(text)) },
                searchResult = state.result,
                query = state.query,
                onCancelSearch = { viewModel.triggerEvent(HomeUiEvents.StopSearch) }
            )
        }
    }
}
## LIBS
* Jetpack Compose: Acelerar a contrução das UIs,
* Coil: Carregar as imagens e fazer o cache, tambem é kotlin frindly e integra bem com o compose.
* Ktor: Requests HTTP, lib Kotlin frindly e divertida
* kotlinx-serialization: Casadinha com ktor client para serialização de requests 
* Navigation Compose: Navegação com compose usando uma DLS legal
* Hilt: Injeção de dependencias masi fácil e entegra bem com compose

## Se eu tivesse mais tempo..
Bom em primeiro lugar eu terminaria a tela de detalhes, teminaria de estruturar a arquitetura, 
escreveria os testes dos viewmodels e dos composables, adicionaria feedback de loading nas telas
e pra fechar com chave de ouro offline caching.

## Como executar a aplicação
* Para contruir o projeto vulgo "buildar" e nescessário a ultima versão do 
  android gradle plugin 7.0.0-beta03 disponível a partir do android studio 2020.3.1 beta Artic Fox
  [Download aqui](https://developer.android.com/studio/preview)
* Pra executar basta ter um dispositivo/emulador com android api 23 ou superior

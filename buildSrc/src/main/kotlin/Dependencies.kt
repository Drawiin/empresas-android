object Sdk {
    const val minSdk = 23
    const val targetSdk = 30
    const val compileSdk = 30
}

object Versions {
    const val gradle = "7.0.0-beta03"
    const val kotlin = "1.5.10"
    const val hilt = "2.35.1"
    const val compose = "1.0.0-beta08"
    const val ktor = "1.5.0"
    const val kotlinSerialization = "1.0.1"
    const val androidx = "1.5.0"
    const val lifecycle = "2.3.1"
    const val activityCompose = "1.3.0-beta01"
    const val material = "1.3.0"
    const val appCompat = "1.3.0"
    const val junit = "1.1.2"
    const val espresso = "3.3.0"
    const val navigationCompose = "2.4.0-alpha02"
    const val hiltNavigationCompose = "1.0.0-alpha02"
}